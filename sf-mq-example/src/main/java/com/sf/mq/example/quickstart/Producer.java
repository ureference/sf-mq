package com.sf.mq.example.quickstart;

import com.sf.mq.client.exception.MQClientException;
import com.sf.mq.client.producer.DefaultMQProducer;
import com.sf.mq.client.producer.SendResult;
import com.sf.mq.common.message.Message;

/**
 * Producer，发送消息
 * 
 */
public class Producer {
    public static void main(String[] args) throws MQClientException, InterruptedException {
        DefaultMQProducer producer = new DefaultMQProducer("QuickStartProducer");
        producer.setNamesrvAddr("127.0.0.1:9876");
        producer.setInstanceName("QuickStartProducer");
        producer.start();

        for (int i = 0; i < 10; i++) {
            try {
                Message msg = new Message("QuickStart",// topic
                        "TagA",// tag
                        ("Hello RocketMQ ,QuickStart" + i).getBytes()// body
                );
                SendResult sendResult = producer.send(msg);
                System.out.println(sendResult);
            }
            catch (Exception e) {
                e.printStackTrace();
                Thread.sleep(1000);
            }
        }

        producer.shutdown();
    }
}
