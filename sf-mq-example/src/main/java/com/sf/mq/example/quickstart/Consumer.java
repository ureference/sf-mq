package com.sf.mq.example.quickstart;

import com.sf.mq.client.consumer.DefaultMQPushConsumer;
import com.sf.mq.client.consumer.listener.ConsumeConcurrentlyContext;
import com.sf.mq.client.consumer.listener.ConsumeConcurrentlyStatus;
import com.sf.mq.client.consumer.listener.MessageListenerConcurrently;
import com.sf.mq.client.exception.MQClientException;
import com.sf.mq.common.consumer.ConsumeFromWhere;
import com.sf.mq.common.message.MessageExt;

import java.util.List;


/**
 * Consumer，订阅消息
 */
public class Consumer {

    public static void main(String[] args) throws InterruptedException, MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("QuickStartConsumer");

        consumer.setNamesrvAddr("127.0.0.1:9876");
        consumer.setInstanceName("QuickStartConsumer");
        consumer.subscribe("QuickStart", "*");

        consumer.registerMessageListener(new MessageListenerConcurrently() {

            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs,
                                                            ConsumeConcurrentlyContext context) {
                System.out.println(Thread.currentThread().getName() + " Receive New Messages: " + msgs);
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });

        consumer.start();

        System.out.println("Consumer Started.");
    }
}
