package com.sf.mq.remoting.api;

import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * CommandCustomHeader
 *
 */
public interface CommandCustomHeader {

    /**
     * checkFields
     *
     * @throws RemotingCommandException
     */
    void checkFields() throws RemotingCommandException;
}
