package com.sf.mq.remoting.api;

import com.sf.mq.remoting.exception.RemotingConnectException;
import com.sf.mq.remoting.exception.RemotingSendRequestException;
import com.sf.mq.remoting.exception.RemotingTimeoutException;
import com.sf.mq.remoting.exception.RemotingTooMuchRequestException;
import com.sf.mq.remoting.netty.NettyRequestProcessor;
import com.sf.mq.remoting.protocol.RemotingCommand;

import java.util.List;
import java.util.concurrent.ExecutorService;


/**
 * 远程通信，Client接口
 *
 */
public interface RemotingClient extends RemotingService {

    /**
     * updateNameServerAddressList
     *
     * @param addrs
     */
    public void updateNameServerAddressList(final List<String> addrs);

    /**
     * getNameServerAddressList
     *
     * @return
     */
    public List<String> getNameServerAddressList();


    /**
     * invokeSync
     *
     * @param addr
     * @param request
     * @param timeoutMillis
     * @return
     * @throws InterruptedException
     * @throws RemotingConnectException
     * @throws RemotingSendRequestException
     * @throws RemotingTimeoutException
     */
    public RemotingCommand invokeSync(final String addr, final RemotingCommand request,
                                      final long timeoutMillis) throws InterruptedException, RemotingConnectException,
            RemotingSendRequestException, RemotingTimeoutException;

    /**
     * invokeAsync
     *
     * @param addr
     * @param request
     * @param timeoutMillis
     * @param invokeCallback
     * @throws InterruptedException
     * @throws RemotingConnectException
     * @throws RemotingTooMuchRequestException
     * @throws RemotingTimeoutException
     * @throws RemotingSendRequestException
     */
    public void invokeAsync(final String addr, final RemotingCommand request, final long timeoutMillis,
                            final InvokeCallback invokeCallback) throws InterruptedException, RemotingConnectException,
            RemotingTooMuchRequestException, RemotingTimeoutException, RemotingSendRequestException;


    /**
     * invokeOneway
     *
     * @param addr
     * @param request
     * @param timeoutMillis
     * @throws InterruptedException
     * @throws RemotingConnectException
     * @throws RemotingTooMuchRequestException
     * @throws RemotingTimeoutException
     * @throws RemotingSendRequestException
     */
    public void invokeOneway(final String addr, final RemotingCommand request, final long timeoutMillis)
            throws InterruptedException, RemotingConnectException, RemotingTooMuchRequestException,
            RemotingTimeoutException, RemotingSendRequestException;


    /**
     * registerProcessor
     *
     * @param requestCode
     * @param processor
     * @param executor
     */
    public void registerProcessor(final int requestCode, final NettyRequestProcessor processor,
                                  final ExecutorService executor);


    /**
     * isChannelWriteable
     *
     * @param addr
     * @return
     */
    public boolean isChannelWriteable(final String addr);
}
