package com.sf.mq.remoting.netty;

import com.sf.mq.remoting.api.InvokeCallback;
import com.sf.mq.remoting.common.SemaphoreReleaseOnlyOnce;
import com.sf.mq.remoting.protocol.RemotingCommand;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;


/***
 * 异步请求应答封装
 *
 */
public class ResponseFuture {
    private volatile RemotingCommand responseCommand;
    private volatile boolean sendRequestOK = true;
    private volatile Throwable cause;
    private final int opaque;
    private final long timeoutMillis;
    private final InvokeCallback invokeCallback;
    private final long beginTimestamp = System.currentTimeMillis();
    private final CountDownLatch countDownLatch = new CountDownLatch(1);

    // 保证信号量至多至少只被释放一次
    private final SemaphoreReleaseOnlyOnce once;

    // 保证回调的callback方法至多至少只被执行一次
    private final AtomicBoolean executeCallbackOnlyOnce = new AtomicBoolean(false);


    /**
     * ResponseFuture
     *
     * @param opaque
     * @param timeoutMillis
     * @param invokeCallback
     * @param once
     */
    public ResponseFuture(int opaque, long timeoutMillis, InvokeCallback invokeCallback,
            SemaphoreReleaseOnlyOnce once) {
        this.opaque = opaque;
        this.timeoutMillis = timeoutMillis;
        this.invokeCallback = invokeCallback;
        this.once = once;
    }

    /**
     * executeInvokeCallback
     *
     */
    public void executeInvokeCallback() {
        if (invokeCallback != null) {
            if (this.executeCallbackOnlyOnce.compareAndSet(false, true)) {
                invokeCallback.operationComplete(this);
            }
        }
    }

    /**
     * release
     *
     */
    public void release() {
        if (this.once != null) {
            this.once.release();
        }
    }


    /**
     * isTimeout
     *
     * @return
     */
    public boolean isTimeout() {
        long diff = System.currentTimeMillis() - this.beginTimestamp;
        return diff > this.timeoutMillis;
    }

    /**
     * waitResponse
     *
     * @param timeoutMillis
     * @return
     * @throws InterruptedException
     */
    public RemotingCommand waitResponse(final long timeoutMillis) throws InterruptedException {
        this.countDownLatch.await(timeoutMillis, TimeUnit.MILLISECONDS);
        return this.responseCommand;
    }

    /**
     * putResponse
     *
     * @param responseCommand
     */
    public void putResponse(final RemotingCommand responseCommand) {
        this.responseCommand = responseCommand;
        this.countDownLatch.countDown();
    }

    /**
     * getBeginTimestamp
     *
     * @return
     */
    public long getBeginTimestamp() {
        return beginTimestamp;
    }

    /**
     * isSendRequestOK
     *
     * @return
     */
    public boolean isSendRequestOK() {
        return sendRequestOK;
    }

    /**
     * setSendRequestOK
     *
     * @param sendRequestOK
     */
    public void setSendRequestOK(boolean sendRequestOK) {
        this.sendRequestOK = sendRequestOK;
    }

    /**
     * getTimeoutMillis
     *
     * @return
     */
    public long getTimeoutMillis() {
        return timeoutMillis;
    }

    /**
     * getInvokeCallback
     *
     * @return
     */
    public InvokeCallback getInvokeCallback() {
        return invokeCallback;
    }

    /**
     * getCause
     *
     * @return
     */
    public Throwable getCause() {
        return cause;
    }

    /**
     * setCause
     *
     * @param cause
     */
    public void setCause(Throwable cause) {
        this.cause = cause;
    }

    /**
     * getResponseCommand
     *
     * @return
     */
    public RemotingCommand getResponseCommand() {
        return responseCommand;
    }

    /**
     * setResponseCommand
     *
     * @param responseCommand
     */
    public void setResponseCommand(RemotingCommand responseCommand) {
        this.responseCommand = responseCommand;
    }

    /**
     * getOpaque
     *
     * @return
     */
    public int getOpaque() {
        return opaque;
    }


    @Override
    public String toString() {
        return "ResponseFuture [responseCommand=" + responseCommand + ", sendRequestOK=" + sendRequestOK
                + ", cause=" + cause + ", opaque=" + opaque + ", timeoutMillis=" + timeoutMillis
                + ", invokeCallback=" + invokeCallback + ", beginTimestamp=" + beginTimestamp
                + ", countDownLatch=" + countDownLatch + "]";
    }
}
