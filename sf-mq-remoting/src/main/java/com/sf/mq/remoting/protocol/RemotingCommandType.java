package com.sf.mq.remoting.protocol;

/**
 * RemotingCommandType
 *
 */
public enum RemotingCommandType {
    REQUEST_COMMAND,
    RESPONSE_COMMAND;
}
