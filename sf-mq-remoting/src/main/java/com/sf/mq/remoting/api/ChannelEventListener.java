package com.sf.mq.remoting.api;

import io.netty.channel.Channel;


/**
 * ChannelEventListener
 *
 */
public interface ChannelEventListener {
    /**
     * onChannelConnect
     *
     * @param remoteAddr
     * @param channel
     */
    void onChannelConnect(final String remoteAddr, final Channel channel);


    /**
     * onChannelClose
     *
     * @param remoteAddr
     * @param channel
     */
    void onChannelClose(final String remoteAddr, final Channel channel);


    /**
     * onChannelException
     *
     * @param remoteAddr
     * @param channel
     */
    void onChannelException(final String remoteAddr, final Channel channel);


    /**
     * onChannelIdle
     *
     * @param remoteAddr
     * @param channel
     */
    void onChannelIdle(final String remoteAddr, final Channel channel);
}
