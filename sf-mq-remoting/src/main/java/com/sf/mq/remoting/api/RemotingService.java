package com.sf.mq.remoting.api;

/**
 * RemotingService
 *
 */
public interface RemotingService {
    /**
     * start
     *
     */
    public void start();

    /**
     * shutdown
     *
     */
    public void shutdown();

    /**
     * registerRPCHook
     *
     * @param rpcHook
     */
    public void registerRPCHook(RPCHook rpcHook);
}
