package com.sf.mq.remoting.api;


import com.sf.mq.remoting.netty.ResponseFuture;

/**
 * 异步调用应答回调接口
 *
 */
public interface InvokeCallback {
    /**
     * operationComplete
     *
     * @param responseFuture
     */
    public void operationComplete(final ResponseFuture responseFuture);
}
