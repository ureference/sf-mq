package com.sf.mq.remoting.api;

import com.sf.mq.remoting.protocol.RemotingCommand;

/**
 * RPCHook
 *
 */
public interface RPCHook {

    /**
     * doBeforeRequest
     *
     * @param remoteAddr
     * @param request
     */
    public void doBeforeRequest(final String remoteAddr, final RemotingCommand request);


    /**
     * doAfterResponse
     *
     * @param remoteAddr
     * @param request
     * @param response
     */
    public void doAfterResponse(final String remoteAddr, final RemotingCommand request, final RemotingCommand response);
}
