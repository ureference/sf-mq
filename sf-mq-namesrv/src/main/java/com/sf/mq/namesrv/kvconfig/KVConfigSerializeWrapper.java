package com.sf.mq.namesrv.kvconfig;


import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashMap;


/**
 * KV配置序列化，json包装
 *
 */
public class KVConfigSerializeWrapper extends RemotingSerializable {
    private HashMap<String/* Namespace */, HashMap<String/* Key */, String/* Value */>> configTable;


    public HashMap<String, HashMap<String, String>> getConfigTable() {
        return configTable;
    }


    public void setConfigTable(HashMap<String, HashMap<String, String>> configTable) {
        this.configTable = configTable;
    }
}
