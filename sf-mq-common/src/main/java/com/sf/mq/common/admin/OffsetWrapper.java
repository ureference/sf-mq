package com.sf.mq.common.admin;

/**
 * Offset包装类，含Broker、Consumer
 *
 */
public class OffsetWrapper {
    private long brokerOffset;
    private long consumerOffset;
    // 消费的最后一条消息对应的时间戳
    private long lastTimestamp;


    public long getBrokerOffset() {
        return brokerOffset;
    }


    public void setBrokerOffset(long brokerOffset) {
        this.brokerOffset = brokerOffset;
    }


    public long getConsumerOffset() {
        return consumerOffset;
    }


    public void setConsumerOffset(long consumerOffset) {
        this.consumerOffset = consumerOffset;
    }


    public long getLastTimestamp() {
        return lastTimestamp;
    }


    public void setLastTimestamp(long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }
}
