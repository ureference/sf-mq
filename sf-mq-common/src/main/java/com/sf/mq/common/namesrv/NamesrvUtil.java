package com.sf.mq.common.namesrv;

/**
 * NamesrvUtil
 *
 */
public class NamesrvUtil {
    public static final String NAMESPACE_ORDER_TOPIC_CONFIG = "ORDER_TOPIC_CONFIG";
    public static final String NAMESPACE_PROJECT_CONFIG = "PROJECT_CONFIG";
}
