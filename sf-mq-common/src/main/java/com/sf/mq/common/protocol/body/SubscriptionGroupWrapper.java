package com.sf.mq.common.protocol.body;

import com.sf.mq.common.DataVersion;
import com.sf.mq.common.subscription.SubscriptionGroupConfig;
import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 订阅组配置，序列化包装
 *
 */
public class SubscriptionGroupWrapper extends RemotingSerializable {
    private ConcurrentHashMap<String, SubscriptionGroupConfig> subscriptionGroupTable =
            new ConcurrentHashMap<String, SubscriptionGroupConfig>(1024);
    private DataVersion dataVersion = new DataVersion();


    public ConcurrentHashMap<String, SubscriptionGroupConfig> getSubscriptionGroupTable() {
        return subscriptionGroupTable;
    }


    public void setSubscriptionGroupTable(
            ConcurrentHashMap<String, SubscriptionGroupConfig> subscriptionGroupTable) {
        this.subscriptionGroupTable = subscriptionGroupTable;
    }


    public DataVersion getDataVersion() {
        return dataVersion;
    }


    public void setDataVersion(DataVersion dataVersion) {
        this.dataVersion = dataVersion;
    }
}
