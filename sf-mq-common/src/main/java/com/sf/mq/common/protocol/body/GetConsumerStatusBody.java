package com.sf.mq.common.protocol.body;

import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashMap;
import java.util.Map;


/**
 * 查看客户端消费组的消费情况。
 *
 */
@Deprecated
public class GetConsumerStatusBody extends RemotingSerializable {
    private Map<MessageQueue, Long> messageQueueTable = new HashMap<MessageQueue, Long>();
    private Map<String, Map<MessageQueue, Long>> consumerTable =
            new HashMap<String, Map<MessageQueue, Long>>();


    public Map<MessageQueue, Long> getMessageQueueTable() {
        return messageQueueTable;
    }


    public void setMessageQueueTable(Map<MessageQueue, Long> messageQueueTable) {
        this.messageQueueTable = messageQueueTable;
    }


    public Map<String, Map<MessageQueue, Long>> getConsumerTable() {
        return consumerTable;
    }


    public void setConsumerTable(Map<String, Map<MessageQueue, Long>> consumerTable) {
        this.consumerTable = consumerTable;
    }
}
