package com.sf.mq.common.protocol.header;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * 重置 offset。
 *
 */
public class ResetOffsetRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String topic;
    @CFNotNull
    private String group;
    @CFNotNull
    private long timestamp;
    @CFNotNull
    private boolean isForce;


    public void checkFields() throws RemotingCommandException {

    }

    public String getTopic() {
        return topic;
    }


    public void setTopic(String topic) {
        this.topic = topic;
    }


    public String getGroup() {
        return group;
    }


    public void setGroup(String group) {
        this.group = group;
    }


    public long getTimestamp() {
        return timestamp;
    }


    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }


    public boolean isForce() {
        return isForce;
    }


    public void setForce(boolean isForce) {
        this.isForce = isForce;
    }
}
