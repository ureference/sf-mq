package com.sf.mq.common.protocol.body;


import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashSet;

/**
 * GroupList
 *
 */
public class GroupList extends RemotingSerializable {
    private HashSet<String> groupList = new HashSet<String>();


    public HashSet<String> getGroupList() {
        return groupList;
    }


    public void setGroupList(HashSet<String> groupList) {
        this.groupList = groupList;
    }
}
