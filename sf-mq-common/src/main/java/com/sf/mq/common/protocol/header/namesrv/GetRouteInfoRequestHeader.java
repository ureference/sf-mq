package com.sf.mq.common.protocol.header.namesrv;

import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * GetRouteInfoRequestHeader
 *
 */
public class GetRouteInfoRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String topic;

    public void checkFields() throws RemotingCommandException {

    }


    public String getTopic() {
        return topic;
    }


    public void setTopic(String topic) {
        this.topic = topic;
    }
}
