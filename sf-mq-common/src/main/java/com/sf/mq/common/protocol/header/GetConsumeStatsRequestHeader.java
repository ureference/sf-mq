package com.sf.mq.common.protocol.header;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * GetConsumeStatsRequestHeader
 *
 */
public class GetConsumeStatsRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String consumerGroup;
    private String topic;

    public void checkFields() throws RemotingCommandException {
        // TODO Auto-generated method stub

    }


    public String getConsumerGroup() {
        return consumerGroup;
    }


    public void setConsumerGroup(String consumerGroup) {
        this.consumerGroup = consumerGroup;
    }


    public String getTopic() {
        return topic;
    }


    public void setTopic(String topic) {
        this.topic = topic;
    }
}
