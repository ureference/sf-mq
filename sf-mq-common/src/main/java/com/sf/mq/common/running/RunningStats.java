package com.sf.mq.common.running;

/**
 * RunningStats
 */
public enum RunningStats {
    commitLogMaxOffset,
    commitLogMinOffset,
    commitLogDiskRatio,
    consumeQueueDiskRatio,
    scheduleMessageOffset,
}
