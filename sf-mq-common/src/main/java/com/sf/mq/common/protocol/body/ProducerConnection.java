package com.sf.mq.common.protocol.body;

import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashSet;

/**
 * ProducerConnection
 *
 */
public class ProducerConnection extends RemotingSerializable {
    private HashSet<Connection> connectionSet = new HashSet<Connection>();


    public HashSet<Connection> getConnectionSet() {
        return connectionSet;
    }


    public void setConnectionSet(HashSet<Connection> connectionSet) {
        this.connectionSet = connectionSet;
    }
}
