package com.sf.mq.common.protocol.header;


import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * EndTransactionResponseHeader
 *
 */
public class EndTransactionResponseHeader implements CommandCustomHeader {

    public void checkFields() throws RemotingCommandException {

    }

}
