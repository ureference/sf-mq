package com.sf.mq.common.protocol.heartbeat;

/**
 * Message model
 *
 */
public enum MessageModel {
    /**
     * broadcast
     */
    BROADCASTING,
    /**
     * clustering
     */
    CLUSTERING;
}
