package com.sf.mq.common.protocol.header.namesrv;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * WipeWritePermOfBrokerResponseHeader
 *
 */
public class WipeWritePermOfBrokerResponseHeader implements CommandCustomHeader {
    @CFNotNull
    private Integer wipeTopicCount;


    public void checkFields() throws RemotingCommandException {
    }


    public Integer getWipeTopicCount() {
        return wipeTopicCount;
    }


    public void setWipeTopicCount(Integer wipeTopicCount) {
        this.wipeTopicCount = wipeTopicCount;
    }
}
