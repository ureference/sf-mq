package com.sf.mq.common.admin;

import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashMap;


/**
 * Topic所有队列的Offset
 *
 */
public class TopicStatsTable extends RemotingSerializable {
    private HashMap<MessageQueue, TopicOffset> offsetTable = new HashMap<MessageQueue, TopicOffset>();


    public HashMap<MessageQueue, TopicOffset> getOffsetTable() {
        return offsetTable;
    }


    public void setOffsetTable(HashMap<MessageQueue, TopicOffset> offsetTable) {
        this.offsetTable = offsetTable;
    }
}
