package com.sf.mq.common.protocol.body;


import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.concurrent.ConcurrentHashMap;


/**
 * Consumer消费进度，序列化包装
 *
 */
public class ConsumerOffsetSerializeWrapper extends RemotingSerializable {
    private ConcurrentHashMap<String/* topic@group */, ConcurrentHashMap<Integer, Long>> offsetTable =
            new ConcurrentHashMap<String, ConcurrentHashMap<Integer, Long>>(512);


    public ConcurrentHashMap<String, ConcurrentHashMap<Integer, Long>> getOffsetTable() {
        return offsetTable;
    }


    public void setOffsetTable(ConcurrentHashMap<String, ConcurrentHashMap<Integer, Long>> offsetTable) {
        this.offsetTable = offsetTable;
    }
}
