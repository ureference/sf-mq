package com.sf.mq.common.protocol.header;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * CloneGroupOffsetRequestHeader
 *
 */
public class CloneGroupOffsetRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String srcGroup;
    @CFNotNull
    private String destGroup;
    private String topic;
    private boolean offline;

    public void checkFields() throws RemotingCommandException {
    }


    public String getDestGroup() {
        return destGroup;
    }


    public void setDestGroup(String destGroup) {
        this.destGroup = destGroup;
    }


    public String getTopic() {
        return topic;
    }


    public void setTopic(String topic) {
        this.topic = topic;
    }


    public String getSrcGroup() {

        return srcGroup;
    }


    public void setSrcGroup(String srcGroup) {
        this.srcGroup = srcGroup;
    }


    public boolean isOffline() {
        return offline;
    }


    public void setOffline(boolean offline) {
        this.offline = offline;
    }
}
