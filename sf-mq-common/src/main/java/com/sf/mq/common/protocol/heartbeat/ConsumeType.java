package com.sf.mq.common.protocol.heartbeat;

/**
 * 消费类型
 *
 */
public enum ConsumeType {
    /**
     * 主动方式消费
     */
    CONSUME_ACTIVELY,
    /**
     * 被动方式消费
     */
    CONSUME_PASSIVELY, ConsumeType,
}
