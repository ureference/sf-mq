package com.sf.mq.common.protocol.header;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.annotation.CFNullable;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * ConsumeMessageDirectlyResultRequestHeader
 *
 */
public class ConsumeMessageDirectlyResultRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String consumerGroup;
    @CFNullable
    private String clientId;
    @CFNullable
    private String msgId;
    @CFNullable
    private String brokerName;


    public void checkFields() throws RemotingCommandException {
    }


    public String getConsumerGroup() {
        return consumerGroup;
    }


    public void setConsumerGroup(String consumerGroup) {
        this.consumerGroup = consumerGroup;
    }


    public String getBrokerName() {
        return brokerName;
    }


    public void setBrokerName(String brokerName) {
        this.brokerName = brokerName;
    }


    public String getClientId() {
        return clientId;
    }


    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


    public String getMsgId() {
        return msgId;
    }


    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
}
