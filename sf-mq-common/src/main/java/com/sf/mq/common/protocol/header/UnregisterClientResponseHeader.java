package com.sf.mq.common.protocol.header;

import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * UnregisterClientResponseHeader
 *
 */
public class UnregisterClientResponseHeader implements CommandCustomHeader {

    /*
     * (non-Javadoc)
     * 
     * @see com.alibaba.rocketmq.remoting.CommandCustomHeader#checkFields()
     */
    public void checkFields() throws RemotingCommandException {

    }

}
