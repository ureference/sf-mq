package com.sf.mq.common.protocol.body;

import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashMap;

/**
 * KVTable
 *
 */
public class KVTable extends RemotingSerializable {
    private HashMap<String, String> table = new HashMap<String, String>();


    public HashMap<String, String> getTable() {
        return table;
    }


    public void setTable(HashMap<String, String> table) {
        this.table = table;
    }
}
