package com.sf.mq.common.protocol.header;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * 查找被纠正的 offset
 *
 */
public class QueryCorrectionOffsetHeader implements CommandCustomHeader {
    private String filterGroups;
    @CFNotNull
    private String compareGroup;
    @CFNotNull
    private String topic;

    public void checkFields() throws RemotingCommandException {
    }


    public String getFilterGroups() {
        return filterGroups;
    }


    public void setFilterGroups(String filterGroups) {
        this.filterGroups = filterGroups;
    }


    public String getCompareGroup() {
        return compareGroup;
    }


    public void setCompareGroup(String compareGroup) {
        this.compareGroup = compareGroup;
    }


    public String getTopic() {
        return topic;
    }


    public void setTopic(String topic) {
        this.topic = topic;
    }
}
