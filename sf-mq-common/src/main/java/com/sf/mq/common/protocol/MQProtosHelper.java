package com.sf.mq.common.protocol;


import com.sf.mq.common.protocol.header.namesrv.RegisterBrokerRequestHeader;
import com.sf.mq.remoting.common.RemotingHelper;
import com.sf.mq.remoting.exception.RemotingConnectException;
import com.sf.mq.remoting.exception.RemotingSendRequestException;
import com.sf.mq.remoting.exception.RemotingTimeoutException;
import com.sf.mq.remoting.protocol.RemotingCommand;
import com.sf.mq.remoting.protocol.RemotingCommandFactory;

/**
 * 协议辅助类
 *
 */
public class MQProtosHelper {
    /**
     * 将Broker地址注册到Name Server
     */
    public static boolean registerBrokerToNameServer(final String nsaddr, final String brokerAddr,
            final long timeoutMillis) {
        RegisterBrokerRequestHeader requestHeader = new RegisterBrokerRequestHeader();
        requestHeader.setBrokerAddr(brokerAddr);

        RemotingCommand request = RemotingCommandFactory.createRequestCommand(RequestCode.REGISTER_BROKER, requestHeader);

        try {
            RemotingCommand response = RemotingHelper.invokeSync(nsaddr, request, timeoutMillis);
            if (response != null) {
                return ResponseCode.SUCCESS == response.getCode();
            }
        }
        catch (RemotingConnectException e) {
            e.printStackTrace();
        }
        catch (RemotingSendRequestException e) {
            e.printStackTrace();
        }
        catch (RemotingTimeoutException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }
}
