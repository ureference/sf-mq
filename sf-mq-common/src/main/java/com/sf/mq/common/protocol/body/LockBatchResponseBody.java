package com.sf.mq.common.protocol.body;

import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashSet;
import java.util.Set;

/**
 * LockBatchResponseBody
 *
 */
public class LockBatchResponseBody extends RemotingSerializable {
    // Lock成功的队列集合
    private Set<MessageQueue> lockOKMQSet = new HashSet<MessageQueue>();


    public Set<MessageQueue> getLockOKMQSet() {
        return lockOKMQSet;
    }


    public void setLockOKMQSet(Set<MessageQueue> lockOKMQSet) {
        this.lockOKMQSet = lockOKMQSet;
    }

}
