package com.sf.mq.common.protocol.header;

import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * QueryMessageResponseHeader
 */
public class QueryMessageResponseHeader implements CommandCustomHeader {
    @CFNotNull
    private Long indexLastUpdateTimestamp;
    @CFNotNull
    private Long indexLastUpdatePhyoffset;


    public void checkFields() throws RemotingCommandException {
    }


    public Long getIndexLastUpdateTimestamp() {
        return indexLastUpdateTimestamp;
    }


    public void setIndexLastUpdateTimestamp(Long indexLastUpdateTimestamp) {
        this.indexLastUpdateTimestamp = indexLastUpdateTimestamp;
    }


    public Long getIndexLastUpdatePhyoffset() {
        return indexLastUpdatePhyoffset;
    }


    public void setIndexLastUpdatePhyoffset(Long indexLastUpdatePhyoffset) {
        this.indexLastUpdatePhyoffset = indexLastUpdatePhyoffset;
    }
}
