package com.sf.mq.common.protocol.header.filtersrv;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * RegisterFilterServerRequestHeader
 *
 *
 */
public class RegisterFilterServerRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String filterServerAddr;

    public void checkFields() throws RemotingCommandException {
    }


    public String getFilterServerAddr() {
        return filterServerAddr;
    }


    public void setFilterServerAddr(String filterServerAddr) {
        this.filterServerAddr = filterServerAddr;
    }
}
