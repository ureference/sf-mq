package com.sf.mq.common.protocol.header.namesrv;

import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * WipeWritePermOfBrokerRequestHeader
 *
 */
public class WipeWritePermOfBrokerRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String brokerName;

    public void checkFields() throws RemotingCommandException {

    }


    public String getBrokerName() {
        return brokerName;
    }


    public void setBrokerName(String brokerName) {
        this.brokerName = brokerName;
    }
}
