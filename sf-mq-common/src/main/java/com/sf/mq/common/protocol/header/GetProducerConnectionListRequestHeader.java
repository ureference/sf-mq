package com.sf.mq.common.protocol.header;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * GetProducerConnectionListRequestHeader
 */
public class GetProducerConnectionListRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String producerGroup;

    public void checkFields() throws RemotingCommandException {
        // To change body of implemented methods use File | Settings | File
        // Templates.
    }


    public String getProducerGroup() {
        return producerGroup;
    }


    public void setProducerGroup(String producerGroup) {
        this.producerGroup = producerGroup;
    }
}
