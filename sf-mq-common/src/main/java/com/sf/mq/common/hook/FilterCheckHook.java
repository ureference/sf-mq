package com.sf.mq.common.hook;

import java.nio.ByteBuffer;


/**
 * 确认消息是否需要过滤 Hook
 *
 */
public interface FilterCheckHook {
    public String hookName();


    public boolean isFilterMatched(final boolean isUnitMode, final ByteBuffer byteBuffer);
}
