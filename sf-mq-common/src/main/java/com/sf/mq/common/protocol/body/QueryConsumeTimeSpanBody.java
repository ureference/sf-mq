package com.sf.mq.common.protocol.body;

import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashSet;
import java.util.Set;

/***
 * 查看客户端消费组的消费情况
 *
 */
public class QueryConsumeTimeSpanBody extends RemotingSerializable {
    Set<QueueTimeSpan> consumeTimeSpanSet = new HashSet<QueueTimeSpan>();


    public Set<QueueTimeSpan> getConsumeTimeSpanSet() {
        return consumeTimeSpanSet;
    }


    public void setConsumeTimeSpanSet(Set<QueueTimeSpan> consumeTimeSpanSet) {
        this.consumeTimeSpanSet = consumeTimeSpanSet;
    }
}
