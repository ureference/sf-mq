package com.sf.mq.common.protocol.header;

import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.List;

/**
 * GetConsumerListByGroupResponseBody
 */
public class GetConsumerListByGroupResponseBody extends RemotingSerializable {
    private List<String> consumerIdList;


    public List<String> getConsumerIdList() {
        return consumerIdList;
    }


    public void setConsumerIdList(List<String> consumerIdList) {
        this.consumerIdList = consumerIdList;
    }
}
