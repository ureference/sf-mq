package com.sf.mq.common.filter.impl;

/**
 * Op
 *
 */
public abstract class Op {

    private String symbol;


    protected Op(String symbol) {
        this.symbol = symbol;
    }


    public String getSymbol() {
        return symbol;
    }


    public String toString() {
        return symbol;
    }
}
