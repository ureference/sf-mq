package com.sf.mq.common.protocol.header;


import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * ViewBrokerStatsDataRequestHeader
 *
 */
public class ViewBrokerStatsDataRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String statsName;
    @CFNotNull
    private String statsKey;

    public void checkFields() throws RemotingCommandException {

    }


    public String getStatsName() {
        return statsName;
    }


    public void setStatsName(String statsName) {
        this.statsName = statsName;
    }


    public String getStatsKey() {
        return statsKey;
    }


    public void setStatsKey(String statsKey) {
        this.statsKey = statsKey;
    }
}
