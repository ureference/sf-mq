package com.sf.mq.common.protocol.body;


import com.sf.mq.common.DataVersion;
import com.sf.mq.common.TopicConfig;
import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.concurrent.ConcurrentHashMap;

/**
 * TopicConfigSerializeWrapper
 */
public class TopicConfigSerializeWrapper extends RemotingSerializable {
    private ConcurrentHashMap<String, TopicConfig> topicConfigTable =
            new ConcurrentHashMap<String, TopicConfig>();
    private DataVersion dataVersion = new DataVersion();


    public ConcurrentHashMap<String, TopicConfig> getTopicConfigTable() {
        return topicConfigTable;
    }


    public void setTopicConfigTable(ConcurrentHashMap<String, TopicConfig> topicConfigTable) {
        this.topicConfigTable = topicConfigTable;
    }


    public DataVersion getDataVersion() {
        return dataVersion;
    }


    public void setDataVersion(DataVersion dataVersion) {
        this.dataVersion = dataVersion;
    }
}
