package com.sf.mq.common.protocol.body;


import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.Map;


/**
 * 重置 offset 处理结果。
 *
 */
public class ResetOffsetBody extends RemotingSerializable {
    private Map<MessageQueue, Long> offsetTable;


    public Map<MessageQueue, Long> getOffsetTable() {
        return offsetTable;
    }


    public void setOffsetTable(Map<MessageQueue, Long> offsetTable) {
        this.offsetTable = offsetTable;
    }
}
