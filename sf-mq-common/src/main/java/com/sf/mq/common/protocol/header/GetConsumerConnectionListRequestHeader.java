package com.sf.mq.common.protocol.header;

import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * GetConsumerConnectionListRequestHeader
 *
 */
public class GetConsumerConnectionListRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String consumerGroup;

    public void checkFields() throws RemotingCommandException {
        // To change body of implemented methods use File | Settings | File
        // Templates.
    }


    public String getConsumerGroup() {
        return consumerGroup;
    }


    public void setConsumerGroup(String consumerGroup) {
        this.consumerGroup = consumerGroup;
    }
}
