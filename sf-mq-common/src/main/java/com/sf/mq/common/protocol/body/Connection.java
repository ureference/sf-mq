package com.sf.mq.common.protocol.body;


import com.sf.mq.remoting.protocol.LanguageCode;

/**
 * Connection
 *
 */
public class Connection {
    private String clientId;
    private String clientAddr;
    private LanguageCode language;
    private int version;


    public String getClientId() {
        return clientId;
    }


    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


    public String getClientAddr() {
        return clientAddr;
    }


    public void setClientAddr(String clientAddr) {
        this.clientAddr = clientAddr;
    }


    public LanguageCode getLanguage() {
        return language;
    }


    public void setLanguage(LanguageCode language) {
        this.language = language;
    }


    public int getVersion() {
        return version;
    }


    public void setVersion(int version) {
        this.version = version;
    }
}
