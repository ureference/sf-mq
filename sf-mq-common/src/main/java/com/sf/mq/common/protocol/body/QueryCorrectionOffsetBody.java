package com.sf.mq.common.protocol.body;

import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.HashMap;
import java.util.Map;


/**
 * QueryCorrectionOffsetBody
 *
 */
public class QueryCorrectionOffsetBody extends RemotingSerializable {
    private Map<Integer, Long> correctionOffsets = new HashMap<Integer, Long>();


    public Map<Integer, Long> getCorrectionOffsets() {
        return correctionOffsets;
    }


    public void setCorrectionOffsets(Map<Integer, Long> correctionOffsets) {
        this.correctionOffsets = correctionOffsets;
    }
}
