package com.sf.mq.common.protocol.header.namesrv;

import com.sf.mq.remoting.annotation.CFNullable;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * GetKVConfigResponseHeader
 *
 */
public class GetKVConfigResponseHeader implements CommandCustomHeader {
    @CFNullable
    private String value;


    public void checkFields() throws RemotingCommandException {
    }


    public String getValue() {
        return value;
    }


    public void setValue(String value) {
        this.value = value;
    }
}
