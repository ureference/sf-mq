package com.sf.mq.common.protocol.header;

import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/***
 * GetBrokerConfigResponseHeader
 *
 */
public class GetBrokerConfigResponseHeader implements CommandCustomHeader {
    @CFNotNull
    private String version;

    public void checkFields() throws RemotingCommandException {
    }


    public String getVersion() {
        return version;
    }


    public void setVersion(String version) {
        this.version = version;
    }
}
