package com.sf.mq.common.filter.impl;

/**
 * Type
 *
 */
public enum Type {
    NULL,
    OPERAND,
    OPERATOR,
    PARENTHESIS,
    SEPAERATOR;
}
