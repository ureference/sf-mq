package com.sf.mq.common.protocol.header;

import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * SearchOffsetResponseHeader
 *
 */
public class SearchOffsetResponseHeader implements CommandCustomHeader {
    @CFNotNull
    private Long offset;


    public void checkFields() throws RemotingCommandException {
    }


    public Long getOffset() {
        return offset;
    }


    public void setOffset(Long offset) {
        this.offset = offset;
    }
}
