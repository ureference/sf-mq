package com.sf.mq.common.protocol.header;

import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/**
 * QueryMessageRequestHeader
 *
 */
public class QueryMessageRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String topic;
    @CFNotNull
    private String key;
    @CFNotNull
    private Integer maxNum;
    @CFNotNull
    private Long beginTimestamp;
    @CFNotNull
    private Long endTimestamp;


    public void checkFields() throws RemotingCommandException {

    }


    public String getTopic() {
        return topic;
    }


    public void setTopic(String topic) {
        this.topic = topic;
    }


    public String getKey() {
        return key;
    }


    public void setKey(String key) {
        this.key = key;
    }


    public Integer getMaxNum() {
        return maxNum;
    }


    public void setMaxNum(Integer maxNum) {
        this.maxNum = maxNum;
    }


    public Long getBeginTimestamp() {
        return beginTimestamp;
    }


    public void setBeginTimestamp(Long beginTimestamp) {
        this.beginTimestamp = beginTimestamp;
    }


    public Long getEndTimestamp() {
        return endTimestamp;
    }


    public void setEndTimestamp(Long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }
}
