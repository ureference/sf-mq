package com.sf.mq.common.protocol.header;

import com.sf.mq.remoting.annotation.CFNotNull;
import com.sf.mq.remoting.api.CommandCustomHeader;
import com.sf.mq.remoting.exception.RemotingCommandException;

/***
 * 删除订阅组请求参数
 *
 */
public class DeleteSubscriptionGroupRequestHeader implements CommandCustomHeader {
    @CFNotNull
    private String groupName;

    public void checkFields() throws RemotingCommandException {
    }


    public String getGroupName() {
        return groupName;
    }


    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
