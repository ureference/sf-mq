package com.sf.mq.common.test;

import com.sf.mq.common.MixAll;
import org.junit.Assert;
import org.junit.Test;

import java.net.InetAddress;
import java.util.List;

public class MixAllTest {

    @Test
    public void test() throws Exception {
        List<String> localInetAddress = MixAll.getLocalInetAddress();
        String local = InetAddress.getLocalHost().getHostAddress();
        Assert.assertTrue(localInetAddress.contains("127.0.0.1"));
        //Assert.assertTrue(localInetAddress.contains(local));
    }
}
