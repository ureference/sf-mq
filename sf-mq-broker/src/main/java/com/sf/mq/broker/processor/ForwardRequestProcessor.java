package com.sf.mq.broker.processor;

import com.sf.mq.broker.BrokerController;
import com.sf.mq.common.constant.LoggerName;
import com.sf.mq.remoting.netty.NettyRequestProcessor;
import com.sf.mq.remoting.protocol.RemotingCommand;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 向Client转发请求，通常用于管理、监控、统计目的
 */
public class ForwardRequestProcessor implements NettyRequestProcessor {
    private static final Logger log = LoggerFactory.getLogger(LoggerName.BrokerLoggerName);

    private final BrokerController brokerController;


    public ForwardRequestProcessor(final BrokerController brokerController) {
        this.brokerController = brokerController;
    }


    public RemotingCommand processRequest(ChannelHandlerContext ctx, RemotingCommand request) {
        // TODO Auto-generated method stub
        return null;
    }
}
