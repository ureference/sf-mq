package com.sf.mq.broker.client;

import io.netty.channel.Channel;

import java.util.List;


/**
 * ConsumerIdsChangeListener
 *
 */
public interface ConsumerIdsChangeListener {
    public void consumerIdsChanged(final String group, final List<Channel> channels);
}
