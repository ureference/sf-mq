package com.sf.mq.broker.pagecache;

import com.sf.mq.store.common.SelectMapedBufferResult;
import io.netty.channel.FileRegion;
import io.netty.util.AbstractReferenceCounted;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;


/**
 *
 */
public class OneMessageTransfer implements FileRegion {
    private final ByteBuffer byteBufferHeader;
    private final SelectMapedBufferResult selectMapedBufferResult;
    private long transfered; // the bytes which was transfered already


    public OneMessageTransfer(ByteBuffer byteBufferHeader, SelectMapedBufferResult selectMapedBufferResult) {
        this.byteBufferHeader = byteBufferHeader;
        this.selectMapedBufferResult = selectMapedBufferResult;
    }


    public long position() {
        return this.byteBufferHeader.position() + this.selectMapedBufferResult.getByteBuffer().position();
    }


    public long count() {
        return this.byteBufferHeader.limit() + this.selectMapedBufferResult.getSize();
    }


    public long transferTo(WritableByteChannel target, long position) throws IOException {
        if (this.byteBufferHeader.hasRemaining()) {
            transfered += target.write(this.byteBufferHeader);
            return transfered;
        } else if (this.selectMapedBufferResult.getByteBuffer().hasRemaining()) {
            transfered += target.write(this.selectMapedBufferResult.getByteBuffer());
            return transfered;
        }

        return 0;
    }

    public int refCnt() {
        return 0;
    }

    public FileRegion retain() {
        return null;
    }

    public FileRegion retain(int i) {
        return null;
    }

    public FileRegion touch() {
        return null;
    }

    public FileRegion touch(Object o) {
        return null;
    }

    public boolean release() {
        return false;
    }

    public boolean release(int i) {
        return false;
    }


    public void close() {
        this.deallocate();
    }


    protected void deallocate() {
        this.selectMapedBufferResult.release();
    }


    public long transfered() {
        return transfered;
    }
}
