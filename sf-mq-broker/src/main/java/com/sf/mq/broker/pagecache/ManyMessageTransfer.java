package com.sf.mq.broker.pagecache;

import com.sf.mq.store.common.GetMessageResult;
import io.netty.channel.FileRegion;
import io.netty.util.AbstractReferenceCounted;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.List;


/**
 *
 */
public class ManyMessageTransfer implements FileRegion {
    private final ByteBuffer byteBufferHeader;
    private final GetMessageResult getMessageResult;
    private long transfered; // the bytes which was transfered already


    public ManyMessageTransfer(ByteBuffer byteBufferHeader, GetMessageResult getMessageResult) {
        this.byteBufferHeader = byteBufferHeader;
        this.getMessageResult = getMessageResult;
    }


    public long position() {
        int pos = byteBufferHeader.position();
        List<ByteBuffer> messageBufferList = this.getMessageResult.getMessageBufferList();
        for (ByteBuffer bb : messageBufferList) {
            pos += bb.position();
        }
        return pos;
    }


    public long count() {
        return byteBufferHeader.limit() + this.getMessageResult.getBufferTotalSize();
    }


    public long transferTo(WritableByteChannel target, long position) throws IOException {
        if (this.byteBufferHeader.hasRemaining()) {
            transfered += target.write(this.byteBufferHeader);
            return transfered;
        } else {
            List<ByteBuffer> messageBufferList = this.getMessageResult.getMessageBufferList();
            for (ByteBuffer bb : messageBufferList) {
                if (bb.hasRemaining()) {
                    transfered += target.write(bb);
                    return transfered;
                }
            }
        }

        return 0;
    }

    public int refCnt() {
        return 0;
    }

    public FileRegion retain() {
        return null;
    }

    public FileRegion retain(int i) {
        return null;
    }

    public FileRegion touch() {
        return null;
    }

    public FileRegion touch(Object o) {
        return null;
    }

    public boolean release() {
        return false;
    }

    public boolean release(int i) {
        return false;
    }


    public void close() {
        this.deallocate();
    }


    protected void deallocate() {
        this.getMessageResult.release();
    }


    public long transfered() {
        return transfered;
    }
}
