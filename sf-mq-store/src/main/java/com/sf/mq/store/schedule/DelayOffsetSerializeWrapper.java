package com.sf.mq.store.schedule;


import com.sf.mq.remoting.protocol.RemotingSerializable;

import java.util.concurrent.ConcurrentHashMap;


/**
 * 延时消息进度，序列化包装
 *
 */
public class DelayOffsetSerializeWrapper extends RemotingSerializable {
    private ConcurrentHashMap<Integer /* level */, Long/* offset */> offsetTable =
            new ConcurrentHashMap<Integer, Long>(32);


    public ConcurrentHashMap<Integer, Long> getOffsetTable() {
        return offsetTable;
    }


    public void setOffsetTable(ConcurrentHashMap<Integer, Long> offsetTable) {
        this.offsetTable = offsetTable;
    }
}
