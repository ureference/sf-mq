package com.sf.mq.store.common;


import com.sf.mq.common.protocol.heartbeat.SubscriptionData;

/**
 * 消息过滤规则实现
 *
 */
public class DefaultMessageFilter implements MessageFilter {

    public boolean isMessageMatched(SubscriptionData subscriptionData, long tagsCode) {
        if (null == subscriptionData) {
            return true;
        }

        if (subscriptionData.isClassFilterMode()){
            return true;
        }

        if (subscriptionData.getSubString() == null) {
            return true;
        }
        if (subscriptionData.getSubString().equals(SubscriptionData.SUB_ALL)) {
            return true;
        }

        return subscriptionData.getCodeSet().contains((int) tagsCode);
    }

}
