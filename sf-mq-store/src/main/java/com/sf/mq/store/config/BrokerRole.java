package com.sf.mq.store.config;

/**
 * Broker角色
 *
 */
public enum BrokerRole {
    // 异步复制Master
    ASYNC_MASTER,
    // 同步双写Master
    SYNC_MASTER,
    // Slave
    SLAVE
}
