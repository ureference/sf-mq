package com.sf.mq.store.common;

import com.sf.mq.common.protocol.heartbeat.SubscriptionData;

/**
 * 消息过滤接口
 *
 */
public interface MessageFilter {
    public boolean isMessageMatched(final SubscriptionData subscriptionData, final long tagsCode);
}
