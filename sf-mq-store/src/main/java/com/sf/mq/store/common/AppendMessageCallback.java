package com.sf.mq.store.common;

import java.nio.ByteBuffer;


/**
 * Write messages callback interface
 *
 */
public interface AppendMessageCallback {

    /**
     * After message serialization, write MapedByteBuffer
     * 
     * @param byteBuffer
     * @param maxBlank
     * @param msg
     * @return How many bytes to write
     */
    public AppendMessageResult doAppend(final long fileFromOffset, final ByteBuffer byteBuffer,
                                        final int maxBlank, final Object msg);
}
