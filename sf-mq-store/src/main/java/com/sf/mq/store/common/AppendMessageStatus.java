package com.sf.mq.store.common;

/**
 * When write a message to the commit log, returns code
 *
 */
public enum AppendMessageStatus {
    PUT_OK,
    END_OF_FILE,
    MESSAGE_SIZE_EXCEEDED,
    UNKNOWN_ERROR,
}
