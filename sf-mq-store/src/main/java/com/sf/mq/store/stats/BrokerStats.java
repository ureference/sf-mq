package com.sf.mq.store.stats;

import com.sf.mq.common.constant.LoggerName;
import com.sf.mq.store.common.DefaultMessageStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Broker上的一些统计数据
 *
 */
public class BrokerStats {
    private static final Logger log = LoggerFactory.getLogger(LoggerName.BrokerLoggerName);

    // 昨天凌晨00:00:00记录的put消息总数
    private volatile long msgPutTotalYesterdayMorning;
    // 今天凌晨00:00:00记录的put消息总数
    private volatile long msgPutTotalTodayMorning;

    // 昨天凌晨00:00:00记录的get消息总数
    private volatile long msgGetTotalYesterdayMorning;
    // 今天凌晨00:00:00记录的get消息总数
    private volatile long msgGetTotalTodayMorning;

    private final DefaultMessageStore defaultMessageStore;


    public BrokerStats(DefaultMessageStore defaultMessageStore) {
        this.defaultMessageStore = defaultMessageStore;
    }


    /**
     * 每天00:00:00调用
     */
    public void record() {
        this.msgPutTotalYesterdayMorning = this.msgPutTotalTodayMorning;
        this.msgGetTotalYesterdayMorning = this.msgGetTotalTodayMorning;

        this.msgPutTotalTodayMorning =
                this.defaultMessageStore.getStoreStatsService().getPutMessageTimesTotal();
        this.msgGetTotalTodayMorning =
                this.defaultMessageStore.getStoreStatsService().getGetMessageTransferedMsgCount().get();

        log.info("yesterday put message total: {}", msgPutTotalTodayMorning - msgPutTotalYesterdayMorning);
        log.info("yesterday get message total: {}", msgGetTotalTodayMorning - msgGetTotalYesterdayMorning);
    }


    public long getMsgPutTotalYesterdayMorning() {
        return msgPutTotalYesterdayMorning;
    }


    public void setMsgPutTotalYesterdayMorning(long msgPutTotalYesterdayMorning) {
        this.msgPutTotalYesterdayMorning = msgPutTotalYesterdayMorning;
    }


    public long getMsgPutTotalTodayMorning() {
        return msgPutTotalTodayMorning;
    }


    public void setMsgPutTotalTodayMorning(long msgPutTotalTodayMorning) {
        this.msgPutTotalTodayMorning = msgPutTotalTodayMorning;
    }


    public long getMsgGetTotalYesterdayMorning() {
        return msgGetTotalYesterdayMorning;
    }


    public void setMsgGetTotalYesterdayMorning(long msgGetTotalYesterdayMorning) {
        this.msgGetTotalYesterdayMorning = msgGetTotalYesterdayMorning;
    }


    public long getMsgGetTotalTodayMorning() {
        return msgGetTotalTodayMorning;
    }


    public void setMsgGetTotalTodayMorning(long msgGetTotalTodayMorning) {
        this.msgGetTotalTodayMorning = msgGetTotalTodayMorning;
    }


    public long getMsgPutTotalTodayNow() {
        return this.defaultMessageStore.getStoreStatsService().getPutMessageTimesTotal();
    }


    public long getMsgGetTotalTodayNow() {
        return this.defaultMessageStore.getStoreStatsService().getGetMessageTransferedMsgCount().get();
    }
}
