package com.sf.mq.store.test;

import com.sf.mq.store.common.StoreCheckpoint;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;


public class StoreCheckpointTest {
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }


    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }


    @Test
    public void test_write_read() {
        try {

            StoreCheckpoint storeCheckpoint = new StoreCheckpoint("./a/b/0000");
            System.out.println(new File("11.txt").getAbsolutePath());
            long physicMsgTimestamp = 0xAABB;
            long logicsMsgTimestamp = 0xCCDD;
            storeCheckpoint.setPhysicMsgTimestamp(physicMsgTimestamp);
            storeCheckpoint.setLogicsMsgTimestamp(logicsMsgTimestamp);
            storeCheckpoint.flush();

            // 因为时间精度问题，所以最小时间向前回退3s
            long diff = physicMsgTimestamp - storeCheckpoint.getMinTimestamp();
            assertTrue(diff == 3000);

            storeCheckpoint.shutdown();

            storeCheckpoint = new StoreCheckpoint("a/b/0000");
            assertTrue(physicMsgTimestamp == storeCheckpoint.getPhysicMsgTimestamp());
            assertTrue(logicsMsgTimestamp == storeCheckpoint.getLogicsMsgTimestamp());
        }
        catch (Throwable e) {
            e.printStackTrace();
            assertTrue(false);
        }

    }
}
