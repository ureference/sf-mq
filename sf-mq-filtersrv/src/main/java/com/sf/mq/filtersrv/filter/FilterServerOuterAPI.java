package com.sf.mq.filtersrv.filter;


import com.sf.mq.client.exception.MQBrokerException;
import com.sf.mq.common.protocol.RequestCode;
import com.sf.mq.common.protocol.ResponseCode;
import com.sf.mq.common.protocol.header.filtersrv.RegisterFilterServerRequestHeader;
import com.sf.mq.common.protocol.header.filtersrv.RegisterFilterServerResponseHeader;
import com.sf.mq.remoting.api.RemotingClient;
import com.sf.mq.remoting.exception.RemotingCommandException;
import com.sf.mq.remoting.exception.RemotingConnectException;
import com.sf.mq.remoting.exception.RemotingSendRequestException;
import com.sf.mq.remoting.exception.RemotingTimeoutException;
import com.sf.mq.remoting.netty.NettyClientConfig;
import com.sf.mq.remoting.netty.NettyRemotingClient;
import com.sf.mq.remoting.protocol.RemotingCommand;
import com.sf.mq.remoting.protocol.RemotingCommandFactory;

/**
 * Broker对外调用的API封装
 *
 */
public class FilterServerOuterAPI {
    private final RemotingClient remotingClient;


    public FilterServerOuterAPI() {
        this.remotingClient = new NettyRemotingClient(new NettyClientConfig());
    }


    public void start() {
        this.remotingClient.start();
    }


    public void shutdown() {
        this.remotingClient.shutdown();
    }


    public RegisterFilterServerResponseHeader registerFilterServerToBroker(//
                                                                           final String brokerAddr,// 1
                                                                           final String filterServerAddr// 2
    ) throws RemotingCommandException, RemotingConnectException, RemotingSendRequestException,
            RemotingTimeoutException, InterruptedException, MQBrokerException {
        RegisterFilterServerRequestHeader requestHeader = new RegisterFilterServerRequestHeader();
        requestHeader.setFilterServerAddr(filterServerAddr);
        RemotingCommand request =
                RemotingCommandFactory.createRequestCommand(RequestCode.REGISTER_FILTER_SERVER, requestHeader);

        RemotingCommand response = this.remotingClient.invokeSync(brokerAddr, request, 3000);
        assert response != null;
        switch (response.getCode()) {
        case ResponseCode.SUCCESS: {
            RegisterFilterServerResponseHeader responseHeader =
                    (RegisterFilterServerResponseHeader) response
                        .decodeCommandCustomHeader(RegisterFilterServerResponseHeader.class);

            return responseHeader;
        }
        default:
            break;
        }

        throw new MQBrokerException(response.getCode(), response.getRemark());
    }
}
