package com.sf.mq.filtersrv.filter.filter;

import com.sf.mq.common.filter.MessageFilter;

/**
 * FilterClassInfo
 *
 */
public class FilterClassInfo {
    private String className;
    private int classCRC;
    private MessageFilter messageFilter;


    public int getClassCRC() {
        return classCRC;
    }


    public void setClassCRC(int classCRC) {
        this.classCRC = classCRC;
    }


    public MessageFilter getMessageFilter() {
        return messageFilter;
    }


    public void setMessageFilter(MessageFilter messageFilter) {
        this.messageFilter = messageFilter;
    }


    public String getClassName() {
        return className;
    }


    public void setClassName(String className) {
        this.className = className;
    }
}
