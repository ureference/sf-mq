package com.sf.mq.filtersrv.filter.filter;

/**
 * FilterClassFetchMethod
 *
 */
public interface FilterClassFetchMethod {
    public String fetch(final String topic, final String consumerGroup, final String className);
}
