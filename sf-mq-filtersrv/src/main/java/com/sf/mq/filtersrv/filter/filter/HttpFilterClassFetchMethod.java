package com.sf.mq.filtersrv.filter.filter;

import com.sf.mq.common.constant.LoggerName;
import com.sf.mq.common.utils.HttpTinyClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * HttpFilterClassFetchMethod
 *
 */
public class HttpFilterClassFetchMethod implements FilterClassFetchMethod {
    private static final Logger log = LoggerFactory.getLogger(LoggerName.FiltersrvLoggerName);
    private final String url;


    public HttpFilterClassFetchMethod(String url) {
        this.url = url;
    }


    public String fetch(String topic, String consumerGroup, String className) {
        String thisUrl = String.format("%s/%s.java", this.url, className);

        try {
            HttpTinyClient.HttpResult result = HttpTinyClient.httpGet(thisUrl, null, null, "UTF-8", 5000);
            if (200 == result.code) {
                return result.content;
            }
        }
        catch (Exception e) {
            log.error(
                String.format("call <%s> exception, Topic: %s Group: %s", thisUrl, topic, consumerGroup), e);
        }

        return null;
    }
}
