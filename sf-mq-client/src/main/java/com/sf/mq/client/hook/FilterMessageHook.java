package com.sf.mq.client.hook;

/**
 * FilterMessageHook
 *
 */
public interface FilterMessageHook {
    public String hookName();


    public void filterMessage(final FilterMessageContext context);
}
