package com.sf.mq.client.consumer;

import com.sf.mq.common.message.MessageQueue;

import java.util.List;


/**
 * Strategy Algorithm for message allocating between consumers
 *
 */
public interface AllocateMessageQueueStrategy {

    /**
     * Allocating by consumer id
     *
     * @param consumerGroup current consumer group
     * @param currentCID    current consumer id
     * @param mqAll         message queue set in current topic
     * @param cidAll        consumer set in current consumer group
     * @return
     */
    public List<MessageQueue> allocate(//
                                       final String consumerGroup,//
                                       final String currentCID,//
                                       final List<MessageQueue> mqAll,//
                                       final List<String> cidAll//
    );


    /**
     * Algorithm name
     *
     * @return
     */
    public String getName();
}
