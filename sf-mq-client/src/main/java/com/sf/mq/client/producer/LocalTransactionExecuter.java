package com.sf.mq.client.producer;


import com.sf.mq.common.message.Message;

/**
 * LocalTransactionExecuter
 *
 */
public interface LocalTransactionExecuter {
    public LocalTransactionState executeLocalTransactionBranch(final Message msg, final Object arg);
}
