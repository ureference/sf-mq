package com.sf.mq.client.impl.consumer;

import com.sf.mq.client.impl.factory.MQClientInstance;
import com.sf.mq.client.log.ClientLogger;
import com.sf.mq.common.ServiceThread;
import org.slf4j.Logger;


/**
 * Rebalance Service
 *
 */
public class RebalanceService extends ServiceThread {
    private final Logger log = ClientLogger.getLog();
    private final MQClientInstance mqClientFactory;


    public RebalanceService(MQClientInstance mqClientFactory) {
        this.mqClientFactory = mqClientFactory;
    }

    private static long WaitInterval = 1000 * 10;


    public void run() {
        log.info(this.getServiceName() + " service started");

        while (!this.isStoped()) {
            this.waitForRunning(WaitInterval);
            this.mqClientFactory.doRebalance();
        }

        log.info(this.getServiceName() + " service end");
    }


    @Override
    public String getServiceName() {
        return RebalanceService.class.getSimpleName();
    }
}
