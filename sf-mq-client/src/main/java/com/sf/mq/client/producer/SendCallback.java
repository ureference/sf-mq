package com.sf.mq.client.producer;

/**
 * SendCallback
 *
 */
public interface SendCallback {
    public void onSuccess(final SendResult sendResult);


    public void onException(final Throwable e);
}
