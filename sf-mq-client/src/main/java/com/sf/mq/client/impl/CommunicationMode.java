package com.sf.mq.client.impl;

/**
 * CommunicationMode
 */
public enum CommunicationMode {
    SYNC,
    ASYNC,
    ONEWAY,
}
