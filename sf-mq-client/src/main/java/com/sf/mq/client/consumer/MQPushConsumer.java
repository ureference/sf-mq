package com.sf.mq.client.consumer;


import com.sf.mq.client.consumer.listener.MessageListenerConcurrently;
import com.sf.mq.client.consumer.listener.MessageListenerOrderly;
import com.sf.mq.client.exception.MQClientException;

/**
 * Push consumer
 *
 */
public interface MQPushConsumer extends MQConsumer {
    /**
     * Start the consumer
     *
     * @throws MQClientException
     */
    void start() throws MQClientException;


    /**
     * Shutdown the consumer
     */
    void shutdown();


    /**
     * Register the message listener
     *
     * @param messageListener
     */
    void registerMessageListener(final MessageListenerConcurrently messageListener);


    void registerMessageListener(final MessageListenerOrderly messageListener);


    /**
     * Subscribe some topic
     *
     * @param topic
     * @param subExpression
     *            subscription expression.it only support or operation such as
     *            "tag1 || tag2 || tag3" <br>
     *            if null or * expression,meaning subscribe all
     * @throws MQClientException
     */
    void subscribe(final String topic, final String subExpression) throws MQClientException;


    /**
     * Subscribe some topic
     *
     * @param topic
     * @param fullClassName
     *            full class name，must extend
     *            com.alibaba.rocketmq.common.filter. MessageFilter
     * @param filterClassSource
     *            class source code，used UTF-8 file encoding,must be responsible
     *            for your code safety
     * @throws MQClientException
     */
    void subscribe(final String topic, final String fullClassName, final String filterClassSource)
            throws MQClientException;


    /**
     * Unsubscribe consumption some topic
     *
     * @param topic
     *            message topic
     */
    void unsubscribe(final String topic);


    /**
     * Update the consumer thread pool size Dynamically
     *
     * @param corePoolSize
     */
    void updateCorePoolSize(int corePoolSize);


    /**
     * Suspend the consumption
     */
    void suspend();


    /**
     * Resume the consumption
     */
    void resume();
}
