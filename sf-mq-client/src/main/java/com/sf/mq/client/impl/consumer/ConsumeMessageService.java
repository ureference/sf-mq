package com.sf.mq.client.impl.consumer;

import com.sf.mq.common.message.MessageExt;
import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.common.protocol.body.ConsumeMessageDirectlyResult;

import java.util.List;


/**
 * ConsumeMessageService
 *
 */
public interface ConsumeMessageService {
    void start();


    void shutdown();


    void updateCorePoolSize(int corePoolSize);


    void incCorePoolSize();


    void decCorePoolSize();


    int getCorePoolSize();


    ConsumeMessageDirectlyResult consumeMessageDirectly(final MessageExt msg, final String brokerName);


    void submitConsumeRequest(//
                              final List<MessageExt> msgs, //
                              final ProcessQueue processQueue, //
                              final MessageQueue messageQueue, //
                              final boolean dispathToConsume);
}
