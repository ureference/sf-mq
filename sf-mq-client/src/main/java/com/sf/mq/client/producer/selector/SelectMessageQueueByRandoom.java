package com.sf.mq.client.producer.selector;


import com.sf.mq.client.producer.MessageQueueSelector;
import com.sf.mq.common.message.Message;
import com.sf.mq.common.message.MessageQueue;

import java.util.List;
import java.util.Random;


/**
 *
 */
public class SelectMessageQueueByRandoom implements MessageQueueSelector {
    private Random random = new Random(System.currentTimeMillis());


    public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
        int value = random.nextInt();
        if (value < 0) {
            value = Math.abs(value);
        }

        value = value % mqs.size();
        return mqs.get(value);
    }
}
