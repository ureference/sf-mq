package com.sf.mq.client.producer;

import com.sf.mq.client.exception.MQClientException;
import com.sf.mq.common.message.Message;
import com.sf.mq.remoting.api.RPCHook;

/**
 * DefaultMQProducer
 *
 */
public class TransactionMQProducer extends DefaultMQProducer {
    private TransactionCheckListener transactionCheckListener;
    private int checkThreadPoolMinSize = 1;
    private int checkThreadPoolMaxSize = 1;
    private int checkRequestHoldMax = 2000;


    public TransactionMQProducer() {
    }


    public TransactionMQProducer(final String producerGroup) {
        super(producerGroup);
    }
    public TransactionMQProducer(final String producerGroup,RPCHook rpcHook) {
        super(producerGroup,rpcHook);
    }

    @Override
    public void start() throws MQClientException {
        this.defaultMQProducerImpl.initTransactionEnv();
        super.start();
    }


    @Override
    public void shutdown() {
        super.shutdown();
        this.defaultMQProducerImpl.destroyTransactionEnv();
    }


    @Override
    public TransactionSendResult sendMessageInTransaction(final Message msg,
            final LocalTransactionExecuter tranExecuter, final Object arg) throws MQClientException {
        if (null == this.transactionCheckListener) {
            throw new MQClientException("localTransactionBranchCheckListener is null", null);
        }

        return this.defaultMQProducerImpl.sendMessageInTransaction(msg, tranExecuter, arg);
    }


    public TransactionCheckListener getTransactionCheckListener() {
        return transactionCheckListener;
    }


    public void setTransactionCheckListener(TransactionCheckListener transactionCheckListener) {
        this.transactionCheckListener = transactionCheckListener;
    }


    public int getCheckThreadPoolMinSize() {
        return checkThreadPoolMinSize;
    }


    public void setCheckThreadPoolMinSize(int checkThreadPoolMinSize) {
        this.checkThreadPoolMinSize = checkThreadPoolMinSize;
    }


    public int getCheckThreadPoolMaxSize() {
        return checkThreadPoolMaxSize;
    }


    public void setCheckThreadPoolMaxSize(int checkThreadPoolMaxSize) {
        this.checkThreadPoolMaxSize = checkThreadPoolMaxSize;
    }


    public int getCheckRequestHoldMax() {
        return checkRequestHoldMax;
    }


    public void setCheckRequestHoldMax(int checkRequestHoldMax) {
        this.checkRequestHoldMax = checkRequestHoldMax;
    }
}
