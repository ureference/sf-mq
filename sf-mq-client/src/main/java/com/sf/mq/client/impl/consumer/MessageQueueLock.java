package com.sf.mq.client.impl.consumer;


import com.sf.mq.common.message.MessageQueue;

import java.util.concurrent.ConcurrentHashMap;


/**
 * Message lock,strictly ensure the single queue only one thread at a time consuming
 * 
 * @author shijia.wxr<vintage.wang@gmail.com>
 * @since 2013-6-25
 */
public class MessageQueueLock {
    private ConcurrentHashMap<MessageQueue, Object> mqLockTable =
            new ConcurrentHashMap<MessageQueue, Object>();


    public Object fetchLockObject(final MessageQueue mq) {
        Object objLock = this.mqLockTable.get(mq);
        if (null == objLock) {
            objLock = new Object();
            Object prevLock = this.mqLockTable.putIfAbsent(mq, objLock);
            if (prevLock != null) {
                objLock = prevLock;
            }
        }

        return objLock;
    }
}
