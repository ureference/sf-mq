package com.sf.mq.client.impl.producer;


import com.sf.mq.client.producer.TransactionCheckListener;
import com.sf.mq.common.message.MessageExt;
import com.sf.mq.common.protocol.header.CheckTransactionStateRequestHeader;

import java.util.Set;


/**
 * MQProducerInner
 *
 */
public interface MQProducerInner {
    Set<String> getPublishTopicList();


    boolean isPublishTopicNeedUpdate(final String topic);


    TransactionCheckListener checkListener();


    void checkTransactionState(//
                               final String addr, //
                               final MessageExt msg, //
                               final CheckTransactionStateRequestHeader checkRequestHeader);


    void updateTopicPublishInfo(final String topic, final TopicPublishInfo info);


    boolean isUnitMode();
}
