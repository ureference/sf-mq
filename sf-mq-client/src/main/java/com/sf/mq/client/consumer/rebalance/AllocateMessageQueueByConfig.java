package com.sf.mq.client.consumer.rebalance;

import com.sf.mq.client.consumer.AllocateMessageQueueStrategy;
import com.sf.mq.common.message.MessageQueue;

import java.util.List;


/**
 *
 */
public class AllocateMessageQueueByConfig implements AllocateMessageQueueStrategy {
    private List<MessageQueue> messageQueueList;


    public String getName() {
        return "CONFIG";
    }


    public List<MessageQueue> allocate(String consumerGroup, String currentCID, List<MessageQueue> mqAll,
                                       List<String> cidAll) {
        return this.messageQueueList;
    }


    public List<MessageQueue> getMessageQueueList() {
        return messageQueueList;
    }


    public void setMessageQueueList(List<MessageQueue> messageQueueList) {
        this.messageQueueList = messageQueueList;
    }
}
