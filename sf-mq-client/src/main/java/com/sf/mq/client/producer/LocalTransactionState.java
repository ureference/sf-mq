package com.sf.mq.client.producer;

/**
 * LocalTransactionState
 *
 */
public enum LocalTransactionState {
    COMMIT_MESSAGE,
    ROLLBACK_MESSAGE,
    UNKNOW,
}
