package com.sf.mq.client.producer;


import com.sf.mq.common.message.Message;
import com.sf.mq.common.message.MessageQueue;

import java.util.List;


/**
 * MessageQueueSelector
 *
 */
public interface MessageQueueSelector {
    MessageQueue select(final List<MessageQueue> mqs, final Message msg, final Object arg);
}
