package com.sf.mq.client.consumer;

/**
 * Async message pulling interface
 *
 */
public interface PullCallback {
    public void onSuccess(final PullResult pullResult);

    public void onException(final Throwable e);
}
