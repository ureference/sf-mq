package com.sf.mq.client.consumer.listener;

/**
 * A MessageListener object is used to receive asynchronously delivered messages.
 *
 */
public interface MessageListener {
}
