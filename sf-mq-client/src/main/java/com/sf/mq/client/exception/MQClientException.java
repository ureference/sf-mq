package com.sf.mq.client.exception;

import com.sf.mq.common.UtilAll;
import com.sf.mq.common.help.FAQUrl;

/**
 * MQClientException
 */
public class MQClientException extends Exception {
    private static final long serialVersionUID = -5758410930844185841L;
    private final int responseCode;
    private final String errorMessage;


    public MQClientException(String errorMessage, Throwable cause) {
        super(FAQUrl.attachDefaultURL(errorMessage), cause);
        this.responseCode = -1;
        this.errorMessage = errorMessage;
    }


    public MQClientException(int responseCode, String errorMessage) {
        super(FAQUrl.attachDefaultURL("CODE: " + UtilAll.responseCode2String(responseCode) + "  DESC: "
                + errorMessage));
        this.responseCode = responseCode;
        this.errorMessage = errorMessage;
    }


    public int getResponseCode() {
        return responseCode;
    }


    public String getErrorMessage() {
        return errorMessage;
    }
}
