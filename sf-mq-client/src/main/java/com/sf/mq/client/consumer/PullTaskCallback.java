package com.sf.mq.client.consumer;

import com.sf.mq.common.message.MessageQueue;

/**
 * PullTaskCallback
 *
 */
public interface PullTaskCallback {
    public void doPullTask(final MessageQueue mq, final PullTaskContext context);
}
