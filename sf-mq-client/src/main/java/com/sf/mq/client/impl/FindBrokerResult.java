package com.sf.mq.client.impl;

/**
 * FindBrokerResult
 */
public class FindBrokerResult {
    private final String brokerAddr;
    private final boolean slave;


    public FindBrokerResult(String brokerAddr, boolean slave) {
        this.brokerAddr = brokerAddr;
        this.slave = slave;
    }


    public String getBrokerAddr() {
        return brokerAddr;
    }


    public boolean isSlave() {
        return slave;
    }
}
