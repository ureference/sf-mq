package com.sf.mq.client;

import com.sf.mq.client.consumer.DefaultMQPullConsumer;
import com.sf.mq.client.log.ClientLogger;
import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;

import java.util.Set;
import java.util.TreeSet;


/**
 *
 */
public class MQHelper {
    /**
     * Reset consumer topic offset according to time
     * 
     * @param messageModel
     *            which model
     * @param instanceName
     *            which instance
     * @param consumerGroup
     *            consumer group
     * @param topic
     *            topic
     * @param timestamp
     *            time
     * @throws Exception
     */
    public static void resetOffsetByTimestamp(//
                                              final MessageModel messageModel,//
                                              final String instanceName,//
                                              final String consumerGroup, //
                                              final String topic, //
                                              final long timestamp) throws Exception {
        final Logger log = ClientLogger.getLog();

        DefaultMQPullConsumer consumer = new DefaultMQPullConsumer(consumerGroup);
        consumer.setInstanceName(instanceName);
        consumer.setMessageModel(messageModel);
        consumer.start();

        Set<MessageQueue> mqs = null;
        try {
            mqs = consumer.fetchSubscribeMessageQueues(topic);
            if (mqs != null && !mqs.isEmpty()) {
                TreeSet<MessageQueue> mqsNew = new TreeSet<MessageQueue>(mqs);
                for (MessageQueue mq : mqsNew) {
                    long offset = consumer.searchOffset(mq, timestamp);
                    if (offset >= 0) {
                        consumer.updateConsumeOffset(mq, offset);
                        log.info("resetOffsetByTimestamp updateConsumeOffset success, {} {} {}",
                            consumerGroup, offset, mq);
                    }
                }
            }
        }
        catch (Exception e) {
            log.warn("resetOffsetByTimestamp Exception", e);
            throw e;
        }
        finally {
            if (mqs != null) {
                consumer.getDefaultMQPullConsumerImpl().getOffsetStore().persistAll(mqs);
            }
            consumer.shutdown();
        }
    }


    public static void resetOffsetByTimestamp(//
            final MessageModel messageModel,//
            final String consumerGroup, //
            final String topic, //
            final long timestamp) throws Exception {
        resetOffsetByTimestamp(messageModel, "DEFAULT", consumerGroup, topic, timestamp);
    }
}
