package com.sf.mq.client.impl.consumer;


import com.sf.mq.client.consumer.AllocateMessageQueueStrategy;
import com.sf.mq.client.consumer.MessageQueueListener;
import com.sf.mq.client.impl.factory.MQClientInstance;
import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.common.protocol.heartbeat.ConsumeType;
import com.sf.mq.common.protocol.heartbeat.MessageModel;

import java.util.List;
import java.util.Set;


/**
 * RebalancePullImpl
 *
 */
public class RebalancePullImpl extends RebalanceImpl {
    private final DefaultMQPullConsumerImpl defaultMQPullConsumerImpl;


    public RebalancePullImpl(DefaultMQPullConsumerImpl defaultMQPullConsumerImpl) {
        this(null, null, null, null, defaultMQPullConsumerImpl);
    }


    public RebalancePullImpl(String consumerGroup, MessageModel messageModel,
                             AllocateMessageQueueStrategy allocateMessageQueueStrategy, MQClientInstance mQClientFactory,
                             DefaultMQPullConsumerImpl defaultMQPullConsumerImpl) {
        super(consumerGroup, messageModel, allocateMessageQueueStrategy, mQClientFactory);
        this.defaultMQPullConsumerImpl = defaultMQPullConsumerImpl;
    }


    public long computePullFromWhere(MessageQueue mq) {
        return 0;
    }


    @Override
    public void dispatchPullRequest(List<PullRequest> pullRequestList) {
    }


    public void messageQueueChanged(String topic, Set<MessageQueue> mqAll, Set<MessageQueue> mqDivided) {
        MessageQueueListener messageQueueListener =
                this.defaultMQPullConsumerImpl.getDefaultMQPullConsumer().getMessageQueueListener();
        if (messageQueueListener != null) {
            try {
                messageQueueListener.messageQueueChanged(topic, mqAll, mqDivided);
            }
            catch (Throwable e) {
                log.error("messageQueueChanged exception", e);
            }
        }
    }


    public boolean removeUnnecessaryMessageQueue(MessageQueue mq, ProcessQueue pq) {
        this.defaultMQPullConsumerImpl.getOffsetStore().persist(mq);
        this.defaultMQPullConsumerImpl.getOffsetStore().removeOffset(mq);
        return true;
    }


    @Override
    public ConsumeType consumeType() {
        return ConsumeType.CONSUME_ACTIVELY;
    }
}
