package com.sf.mq.client.consumer.listener;

/**
 * ConsumeConcurrentlyStatus
 *
 */
public enum ConsumeConcurrentlyStatus {
    /**
     * Success consumption
     */
    CONSUME_SUCCESS,
    /**
     * Failure consumption,later try to consume
     */
    RECONSUME_LATER;
}
