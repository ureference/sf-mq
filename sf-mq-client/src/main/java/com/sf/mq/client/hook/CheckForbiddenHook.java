package com.sf.mq.client.hook;


import com.sf.mq.client.exception.MQClientException;

/**
 * CheckForbiddenHook
 */
public interface CheckForbiddenHook {
    public String hookName();


    public void checkForbidden(final CheckForbiddenContext context) throws MQClientException;
}
