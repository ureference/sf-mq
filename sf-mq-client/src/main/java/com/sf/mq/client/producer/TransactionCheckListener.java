package com.sf.mq.client.producer;


import com.sf.mq.common.message.MessageExt;

/**
 * TransactionCheckListener
 *
 */
public interface TransactionCheckListener {
    LocalTransactionState checkLocalTransactionState(final MessageExt msg);
}
