package com.sf.mq.client.impl.consumer;


import com.sf.mq.common.consumer.ConsumeFromWhere;
import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.common.protocol.body.ConsumerRunningInfo;
import com.sf.mq.common.protocol.heartbeat.ConsumeType;
import com.sf.mq.common.protocol.heartbeat.MessageModel;
import com.sf.mq.common.protocol.heartbeat.SubscriptionData;

import java.util.Set;


/**
 * Consumer inner interface
 *
 */
public interface MQConsumerInner {
    String groupName();


    MessageModel messageModel();


    ConsumeType consumeType();


    ConsumeFromWhere consumeFromWhere();


    Set<SubscriptionData> subscriptions();


    void doRebalance();


    void persistConsumerOffset();


    void updateTopicSubscribeInfo(final String topic, final Set<MessageQueue> info);


    boolean isSubscribeTopicNeedUpdate(final String topic);


    boolean isUnitMode();


    ConsumerRunningInfo consumerRunningInfo();
}
