package com.sf.mq.client.producer;


import com.sf.mq.client.MQAdmin;
import com.sf.mq.client.exception.MQBrokerException;
import com.sf.mq.client.exception.MQClientException;
import com.sf.mq.common.message.Message;
import com.sf.mq.common.message.MessageQueue;
import com.sf.mq.remoting.exception.RemotingException;

import java.util.List;


/**
 * MQProducer
 *
 */
public interface MQProducer extends MQAdmin {
     void start() throws MQClientException;

     void shutdown();


     List<MessageQueue> fetchPublishMessageQueues(final String topic) throws MQClientException;


     SendResult send(final Message msg) throws MQClientException, RemotingException, MQBrokerException,
            InterruptedException;


     SendResult send(final Message msg, final long timeout) throws MQClientException,
            RemotingException, MQBrokerException, InterruptedException;


     void send(final Message msg, final SendCallback sendCallback) throws MQClientException,
            RemotingException, InterruptedException;


     void send(final Message msg, final SendCallback sendCallback, final long timeout)
            throws MQClientException, RemotingException, InterruptedException;


     void sendOneway(final Message msg) throws MQClientException, RemotingException,
            InterruptedException;


     SendResult send(final Message msg, final MessageQueue mq) throws MQClientException,
            RemotingException, MQBrokerException, InterruptedException;


     SendResult send(final Message msg, final MessageQueue mq, final long timeout)
            throws MQClientException, RemotingException, MQBrokerException, InterruptedException;


     void send(final Message msg, final MessageQueue mq, final SendCallback sendCallback)
            throws MQClientException, RemotingException, InterruptedException;


     void send(final Message msg, final MessageQueue mq, final SendCallback sendCallback, long timeout)
            throws MQClientException, RemotingException, InterruptedException;


     void sendOneway(final Message msg, final MessageQueue mq) throws MQClientException,
            RemotingException, InterruptedException;


     SendResult send(final Message msg, final MessageQueueSelector selector, final Object arg)
            throws MQClientException, RemotingException, MQBrokerException, InterruptedException;


     SendResult send(final Message msg, final MessageQueueSelector selector, final Object arg,
                     final long timeout) throws MQClientException, RemotingException, MQBrokerException,
            InterruptedException;


     void send(final Message msg, final MessageQueueSelector selector, final Object arg,
               final SendCallback sendCallback) throws MQClientException, RemotingException,
            InterruptedException;


     void send(final Message msg, final MessageQueueSelector selector, final Object arg,
               final SendCallback sendCallback, final long timeout) throws MQClientException, RemotingException,
            InterruptedException;


     void sendOneway(final Message msg, final MessageQueueSelector selector, final Object arg)
            throws MQClientException, RemotingException, InterruptedException;


     TransactionSendResult sendMessageInTransaction(final Message msg,
                                                    final LocalTransactionExecuter tranExecuter, final Object arg) throws MQClientException;
}
